import CarShowList from "./CarShowList";

const ImageShow = ({ images }) => {
  const rerenderImages = images.map((image) => {
    return <CarShowList image={image} />;
  });
  return <div className="image-list">{rerenderImages}</div>;
};

export default ImageShow;
