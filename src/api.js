import axios from "axios";

const searchImage = async (term) => {
  const response = await axios.get("https://api.unsplash.com/search/photos", {
    headers: {
      Authorization: "Client-ID mTedAC2XMZ7thcC3RJiL9bLyRayQlgjaFeZ7KDo9IrI",
    },
    params: {
      query: term,
    },
  });
  return response.data.results;
};

export default searchImage;
