import "bulma/css/bulma.css";
import { useState } from "react";
import SearchBar from "./components/SearchBar";
import "./App.css";
import searchImage from "./api";
import ImageShow from "./components/ImageShow";

function App() {
  const [images, setImages] = useState([]);
  const handleSubmit = async (term) => {
    const result = await searchImage(term);
    setImages(result);
  };
  return (
    <>
      <SearchBar onSubmit={handleSubmit} />
      <ImageShow images={images} />
    </>
  );
}

export default App;
